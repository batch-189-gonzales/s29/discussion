db.inventory.insertMany([
	{
		"name": "JavaScript for Beginners",
		"author": "James Doe",
		"price": 5000,
		"stocks": 50,
		"publisher": "JS Publishing House"
	},
	{
		"name": "HTML and CSS",
		"author": "John Thomas",
		"price": 2500,
		"stocks": 38,
		"publisher": "NY Publishers"
	},
	{
		"name": "Web Development Fundamentals",
		"author": "Noah Jimenez",
		"price": 3000,
		"stocks": 10,
		"publisher": "Big Idea Publishing"
	},
	{
		"name": "Java Programming",
		"author": "David Michael",
		"price": 10000,
		"stocks": 100,
		"publisher": "JS Publishing House"
	}
]);

//Comparison Query Operators
/*
	$gt (greater than) or $gte (greater than or equal)

	Syntax:
		db.collectionName.find({"field": {$gt: value}});
		db.collectionName.find({"field": {$gte: value}});
*/

db.inventory.find({
	"stocks": {
		$gt: 50
	}
});

db.inventory.find({
	"stocks": {
		$gte: 50
	}
});

/*
	$lt (less than) or $lte (less than or equal)

	Syntax:
		db.collectionName.find({"field": {$lt: value}});
		db.collectionName.find({"field": {$lte: value}});
*/

db.inventory.find({
	"stocks": {
		$lt: 50
	}
});

db.inventory.find({
	"stocks": {
		$lte: 50
	}
});

/*
	$ne (not equal)

	Syntax:
		db.collectionName.find({"field": {$ne: value}});
*/

db.inventory.find({
	"stocks": {
		$ne: 50
	}
});

/*
	$eq (equal)

	Syntax:
		db.collectionName.find({"field": {$eq: value}});
*/

db.inventory.find({
	"stocks": {
		$eq: 50
	}
});

/*
	$in 

	Syntax:
		db.collectionName.find({"field": {$in: [value1, value2, ..., valueN]}});
*/

db.inventory.find({
	"price": {
		$in: [5000, 10000]
	}
});

/*
	$nin (not in) 

	Syntax:
		db.collectionName.find({"field": {$nin: [value1, value2, ..., valueN]}});
*/

db.inventory.find({
	"price": {
		$nin: [5000, 10000]
	}
});


//Mini Activity
db.inventory.find({
	"price": {
		$lte: 4000
	}
});

db.inventory.find({
	"stocks": {
		$in: [50,100]
	}
});

//Logical Query Operator

/*
	$or

	Syntax:
		db.collectionName.find({$or:[
			{"field1": value1},
			{"field2": value2},
			...
			{"fieldN": valueN}
		]});
*/

db.inventory.find({$or:[
	{"name": "HTML and CSS"},
	{"publisher": "JS Publishing House"}
]});

db.inventory.find({$or:[
	{"author": "James Doe"},
	{"price": {$lte: 5000}}
]});

/*
	$and

	Syntax:
		db.collectionName.find({$and:[
			{"field1": value1},
			{"field2": value2},
			...
			{"fieldN": valueN}
		]});
*/

db.inventory.find({$and:[
	{"stocks": {$ne: 50}},
	{"price": {$ne: 5000}}
]});

//Field Projection

/*
	Inclusion

	Syntax:
		db.collectionName.find({criteria}, {field1: 1, field2: 1, ..., fieldN: 1})
*/

db.inventory.find(
	{
	"publisher": "JS Publishing House"
	},
	{
		"name": 1,
		"author": 1,
		"price":1
	}
);

/*
	Exclusion

	Syntax:
		db.collectionName.find({criteria}, {field1: 0, field2: 0, ..., fieldN: 0})
*/

db.inventory.find(
	{
	"author": "Noah Jimenez"
	},
	{
		"price": 0,
		"stocks": 0
	}
);

//Evaluation Query Operator

/*
	$regex

	Syntax:
		db.collectionName.find({field: {$regex: 'pattern', $options: 'optionsValue'}});

		optionsValue:
			$i - case insensitive
			$m - 
			$x 
			$s -

		Source: https://www.mongodb.com/docs/manual/reference/operator/query/regex/
*/

db.inventory.find({
	"author": {
		$regex: 'M'
	}
});

db.inventory.find({
	"author": {
		$regex: 'M',
		$options: '$i'
	}
});